import React, { Component } from 'react';
import Input from './input'
import Button from './button'
import axios from 'axios'
// import { Link, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import '../App.css';
// import Login from './login'
class SignUp extends Component {
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:''
        }
    }
    //on entering the value in input field and setting to state
    changeEvent(event) { 
        // console.log(event.target.name);
        // console.log(event.target.value);
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
        // console.log(this.state);
};
//submit event 
    submitEvent(event) {
        //preventing the default value of submit button and sending the data from data base.
        event.preventDefault();
        console.log(this.state);
        axios.post(`http://localhost:9090/user/adduser`,this.state)
            .then(function (response) {
                console.log(response);
               

            })
            .catch(function (error) {
                console.log(error)
            })
    };
    render() {
        return (
                <div className="signup">
                <form onSubmit={this.submitEvent.bind(this)} className="form" >
                    <h1>SignUp</h1>
                    <hr />
                    <Input Name="username" Type="text" event={this.changeEvent.bind(this)} />
                    <Input Name="password" Type="password" event={this.changeEvent.bind(this)}/>
                    <hr />
                    <Button Type="submit" Value="SignUp" Id="signupBtn"  />
                </form>
                </div>
            )
    }
}
    export default SignUp